package br.com.alura.argentum.modelo;

import java.time.LocalDateTime;
import java.util.Objects;

public final class Negociacao {

  private final double preco;
  private final int quantidade;
  private final LocalDateTime data;

  public Negociacao(double preco, int quantidade, LocalDateTime data) {
    if (preco < 0) {
      throw new IllegalArgumentException("O preco nao pode ser negativo");
    }

    if (quantidade < 1) {
      throw new IllegalArgumentException("A quantidade deve ser um valor positivo");
    }

    if (data == null) {
      throw new IllegalArgumentException("A data nao pode ser nula");
    }

    this.preco = preco;
    this.quantidade = quantidade;
    this.data = data;
  }

  public double getPreco() {
    return preco;
  }

  public int getQuantidade() {
    return quantidade;
  }

  public LocalDateTime getData() {
    return data;
  }

  public double getVolume() {
    return this.preco * this.quantidade;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Negociacao that = (Negociacao) o;
    return Double.compare(that.preco, preco) == 0 &&
        quantidade == that.quantidade &&
        Objects.equals(data, that.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(preco, quantidade, data);
  }

  public boolean isMesmoDia(LocalDateTime outraData) {
    return this.data.getDayOfMonth() == outraData.getDayOfMonth() &&
        this.data.getMonth() == outraData.getMonth() &&
        this.data.getYear() == outraData.getYear();
  }
}
