package br.com.alura.argentum.reader;

import br.com.alura.argentum.modelo.Negociacao;
import br.com.alura.argentum.xstream.LocalDateTimeConverter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.InputStream;
import java.util.List;

public class LeitorXml {

  public List<Negociacao> carrega(InputStream stream) {

    XStream xStream = new XStream(new DomDriver());
    xStream.registerLocalConverter(Negociacao.class, "data", new LocalDateTimeConverter());
    xStream.alias("negociacao", Negociacao.class);
    return (List<Negociacao>) xStream.fromXML(stream);
  }

}
