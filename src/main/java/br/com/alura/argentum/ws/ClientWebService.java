package br.com.alura.argentum.ws;

import br.com.alura.argentum.modelo.Negociacao;
import br.com.alura.argentum.reader.LeitorXml;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class ClientWebService {

  public static final String URL_WEB_SERVICE = "http://argentumws.caelum.com.br/negociacoes";
  private HttpURLConnection connection = null;

  public static void main(String[] args) {
    ClientWebService ws = new ClientWebService();
    List<Negociacao> negociacoes = ws.getNegociacoes();
    for (Negociacao negociacao : negociacoes) {
      System.out.println(negociacao.getPreco());
    }
  }

  public List<Negociacao> getNegociacoes() {
    try {
      URL url = new URL(URL_WEB_SERVICE);
      connection = (HttpURLConnection) url.openConnection();
      InputStream content = connection.getInputStream();
      return new LeitorXml().carrega(content);
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      connection.disconnect();
    }
  }
}
