package br.com.alura.argentum.bean;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class OlaMundoBean {

  private String mensagem = "It Works From Bean !";
  private String nome;

  public String getMensagem() {
    return mensagem;
  }

  public String getNome() {
    return this.nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public void botaoClicado() {
    System.out.println("O botão foi clicado ! Seu nome é " + this.nome);
  }
}
