package br.com.alura.argentum.bean;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.chart.LineChartModel;
import br.com.alura.argentum.graficos.GeradorDeModeloGrafico;
import br.com.alura.argentum.indicadores.IndicadorFactory;
import br.com.alura.argentum.modelo.Candle;
import br.com.alura.argentum.modelo.CandleFactory;
import br.com.alura.argentum.modelo.Negociacao;
import br.com.alura.argentum.modelo.SerieTemporal;
import br.com.alura.argentum.ws.ClientWebService;

@ViewScoped
@ManagedBean
public class ArgentumBean implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 66453571557344505L;

  private LineChartModel modeloGrafico;
  private List<Negociacao> negociacoes;
  private String nomeMedia;
  private String nomeIndicadorBase;

  public String getNomeMedia() {
    return nomeMedia;
  }

  public void setNomeMedia(String nomeMedia) {
    this.nomeMedia = nomeMedia;
  }

  public String getNomeIndicadorBase() {
    return nomeIndicadorBase;
  }

  public void setNomeIndicadorBase(String nomeIndicadorBase) {
    this.nomeIndicadorBase = nomeIndicadorBase;
  }

  public ArgentumBean() {
    this.negociacoes = new ClientWebService().getNegociacoes();
    geraGrafico();
  }

  public void geraGrafico() {

    List<Candle> candles = new CandleFactory().constroiCandles(this.negociacoes);

    SerieTemporal serie = new SerieTemporal(candles);

    GeradorDeModeloGrafico geradorDeModelo =
        new GeradorDeModeloGrafico(serie, 2, serie.getUltimaPosicao());

    IndicadorFactory fabrica = new IndicadorFactory(nomeMedia, nomeIndicadorBase);

    geradorDeModelo.plotaIndicador(fabrica.defineIndicador());

    this.modeloGrafico = geradorDeModelo.getModeloGrafico();

  }


  public List<Negociacao> getNegociacoes() {
    return this.negociacoes;
  }

  public LineChartModel getModeloGrafico() {
    return modeloGrafico;
  }

}
