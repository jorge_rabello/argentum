package br.com.alura.argentum.xstream;

import static org.junit.Assert.assertEquals;

import br.com.alura.argentum.modelo.Negociacao;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.time.LocalDateTime;
import org.junit.Test;

public class LocalDateTimeConverterTest {

  @Test
  public void deveRetornarUmXmlComADataCorreta() {
    LocalDateTime hoje = LocalDateTime.of(2016, 4, 4, 12, 00);
    Negociacao negociacao = new Negociacao(10.0, 40, hoje);

    XStream xStream = new XStream(new DomDriver());
    xStream.alias("negociacao", Negociacao.class);

    xStream.registerLocalConverter(Negociacao.class, "data", new LocalDateTimeConverter());

    String xmlConvertido = xStream.toXML(negociacao);

    String xmlEsperado =
        "<negociacao>\n"
            + "  <preco>10.0</preco>\n"
            + "  <quantidade>40</quantidade>\n"
            + "  <data>\n"
            + "    <time>1459782000000</time>\n"
            + "    <timezone>America/Sao_Paulo</timezone>\n"
            + "  </data>\n"
            + "</negociacao>";

    assertEquals(xmlEsperado, xmlConvertido);
  }

  @Test
  public void deveConverterUmXmlParaUmaNegociacaoCorreta() {

    String xml =
        "<negociacao>\n"
            + "  <preco>10.0</preco>\n"
            + "  <quantidade>40</quantidade>\n"
            + "  <data>\n"
            + "    <time>1459782000000</time>\n"
            + "    <timezone>America/Sao_Paulo</timezone>\n"
            + "  </data>\n"
            + "</negociacao>";

    XStream xStream = new XStream(new DomDriver());
    xStream.alias("negociacao", Negociacao.class);

    xStream.registerLocalConverter(Negociacao.class, "data", new LocalDateTimeConverter());

    Negociacao negociacaoGerada = (Negociacao) xStream.fromXML(xml);

    LocalDateTime hoje = LocalDateTime.of(2016, 4, 4, 12, 00);
    Negociacao negociacaoEsperada = new Negociacao(10.0, 40, hoje);

    assertEquals(negociacaoEsperada, negociacaoGerada);
  }
}
