package br.com.alura.argentum.modelo;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import org.junit.Test;

public class NegociacaoTest {

  @Test(expected = IllegalArgumentException.class)
  public void naoDeveCriarNegciacaoComPrecoNegativo() {
    new Negociacao(-20.0, 3, LocalDateTime.now());
  }

  @Test(expected = IllegalArgumentException.class)
  public void naoDeveCriarNegciacaoComDataNula() {
    new Negociacao(20.0, 3, null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void naoDeveCriarNegciacaoComQuantidadeNegativa() {
    new Negociacao(20.0, -2, LocalDateTime.now());
  }

  @Test
  public void mesmoSegundoEhMesmoDia() {
    LocalDateTime hoje = LocalDateTime.now();
    LocalDateTime agora = hoje;
    Negociacao negociacao = new Negociacao(100, 20, hoje);
    assertTrue(negociacao.isMesmoDia(agora));
  }

  @Test
  public void horarioDiferenteEhMesmoDia() {
    LocalDateTime hoje = LocalDateTime.of(2016, 04, 04, 12, 00);
    LocalDateTime agora = LocalDateTime.of(2016, 04, 04, 13, 00);

    Negociacao negociacao = new Negociacao(100, 20, hoje);
    assertTrue(negociacao.isMesmoDia(agora));
  }

  @Test
  public void mesDiferenteNaoEhMesmoDia() {
    LocalDateTime hoje = LocalDateTime.of(2016, 04, 04, 12, 00);
    LocalDateTime agora = LocalDateTime.of(2016, 03, 04, 13, 00);

    Negociacao negociacao = new Negociacao(100, 20, hoje);
    assertFalse(negociacao.isMesmoDia(agora));
  }

  @Test
  public void anoDiferenteNaoEhMesmoDia() {
    LocalDateTime hoje = LocalDateTime.of(2016, 04, 04, 12, 00);
    LocalDateTime agora = LocalDateTime.of(2017, 04, 04, 13, 00);

    Negociacao negociacao = new Negociacao(100, 20, hoje);
    assertFalse(negociacao.isMesmoDia(agora));
  }

}
