package br.com.alura.argentum.modelo;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class CandleFactoryTest {

  @Test
  public void sequenciaDeNegociacoesSimples() {
    LocalDateTime hoje = LocalDateTime.now();

    Negociacao negociacao1 = new Negociacao(40.0, 100, hoje);
    Negociacao negociacao2 = new Negociacao(35.0, 100, hoje);
    Negociacao negociacao3 = new Negociacao(45.0, 100, hoje);
    Negociacao negociacao4 = new Negociacao(20.0, 100, hoje);

    List<Negociacao> negociacoes = asList(negociacao1, negociacao2, negociacao3, negociacao4);

    CandleFactory fabrica = new CandleFactory();
    Candle candle = fabrica.geraCandleParaData(negociacoes, hoje);

    assertEquals(20.0, candle.getMinimo(), 0.00001);
    assertEquals(45.0, candle.getMaximo(), 0.00001);
    assertEquals(40.0, candle.getAbertura(), 0.00001);
    assertEquals(20.0, candle.getFechamento(), 0.00001);
    assertEquals(14000.0, candle.getVolume(), 0.00001);

  }

  @Test
  public void geraCandlestickComApenasUmaNegociacao() {
    LocalDateTime hoje = LocalDateTime.now();

    Negociacao negociacao1 = new Negociacao(40.0, 100, hoje);
    List<Negociacao> negociacoes = asList(negociacao1);

    CandleFactory fabrica = new CandleFactory();
    Candle candle = fabrica.geraCandleParaData(negociacoes, hoje);

    assertEquals(40.0, candle.getMinimo(), 0.00001);
    assertEquals(40.0, candle.getMaximo(), 0.00001);
    assertEquals(40.0, candle.getAbertura(), 0.00001);
    assertEquals(40.0, candle.getFechamento(), 0.00001);
    assertEquals(4000.0, candle.getVolume(), 0.00001);

  }

  @Test
  public void geraCandlesticksComZeroEmCasoDeNenhumaNegociacao() {

    LocalDateTime hoje = LocalDateTime.now();
    List<Negociacao> negociacoes = new ArrayList<>();

    CandleFactory fabrica = new CandleFactory();
    Candle candle = fabrica.geraCandleParaData(negociacoes, hoje);

    assertEquals(0.0, candle.getMinimo(), 0.00001);
    assertEquals(0.0, candle.getMaximo(), 0.00001);
    assertEquals(0.0, candle.getAbertura(), 0.00001);
    assertEquals(0.0, candle.getFechamento(), 0.00001);
    assertEquals(0.0, candle.getVolume(), 0.00001);

  }

  @Test
  public void negociacaoDeTresDiasDiferentesGeraTresCandlesDiferentes() {
    LocalDateTime hoje = LocalDateTime.now();
    Negociacao negociacao1 = new Negociacao(50.0, 20, hoje);
    Negociacao negociacao2 = new Negociacao(100.0, 20, hoje);
    Negociacao negociacao3 = new Negociacao(150.0, 20, hoje);

    LocalDateTime amanha = hoje.plusDays(1);
    Negociacao negociacao4 = new Negociacao(50.0, 100, amanha);
    Negociacao negociacao5 = new Negociacao(10.0, 20, amanha);

    LocalDateTime depois = amanha.plusDays(1);
    Negociacao negociacao6 = new Negociacao(35.0, 20, depois);
    Negociacao negociacao7 = new Negociacao(35.0, 20, depois);

    List<Negociacao> negociacoes = asList(
        negociacao1, negociacao2, negociacao3,
        negociacao4, negociacao5, negociacao6, negociacao7);

    CandleFactory fabrica = new CandleFactory();

    List<Candle> candleSticks = fabrica.constroiCandles(negociacoes);

    assertEquals(3, candleSticks.size());
    assertTrue(negociacoes.get(0).isMesmoDia(candleSticks.get(0).getData()));
    assertTrue(negociacoes.get(3).isMesmoDia(candleSticks.get(1).getData()));
    assertTrue(negociacoes.get(5).isMesmoDia(candleSticks.get(2).getData()));

    assertEquals(6000.0, candleSticks.get(0).getVolume(), 0.00001);
    assertEquals(50.0, candleSticks.get(0).getMinimo(), 0.00001);
    assertEquals(150.0, candleSticks.get(0).getMaximo(), 0.00001);
    assertEquals(50.0, candleSticks.get(0).getAbertura(), 0.00001);
    assertEquals(150.0, candleSticks.get(0).getFechamento(), 0.00001);

  }
}

