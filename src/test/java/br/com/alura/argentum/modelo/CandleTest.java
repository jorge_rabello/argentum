package br.com.alura.argentum.modelo;

import static org.junit.Assert.assertTrue;
import java.time.LocalDateTime;
import org.junit.Test;

public class CandleTest {

  @Test(expected = IllegalArgumentException.class)
  public void maximoNaoDeveSerMenorQueMinimo() {

    LocalDateTime hoje = LocalDateTime.now();

    CandleBuilder builder = new CandleBuilder();

    builder.comAbertura(10.0).comFechamento(30.0).comMinimo(25.0).comMaximo(15.0).comVolume(200.0)
        .comData(hoje).geraCandle();
  }


  @Test
  public void eAltaSeFechamentoForIgualAbertura() {
    LocalDateTime hoje = LocalDateTime.now();

    CandleBuilder builder = new CandleBuilder();

    Candle candle = builder.comAbertura(30.0).comFechamento(30.0).comMinimo(10.0).comMaximo(50.0)
        .comVolume(200.0).comData(hoje).geraCandle();

    assertTrue(candle.isAlta());

  }

}
